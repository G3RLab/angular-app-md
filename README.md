# Angular-App-md
Angular Material provides a set of reusable, well-tested and accessible UI components based on Google’s Material Design specification. This course will teach you how to build aesthetic, responsive websites using the angular material library. First, you'll learn the core concepts of material design. Next, you'll touch on developing your environment and setting up the components and services in your library. Finally, you will know how to build forms that validate user input. By the end of this course, you will have an understanding of the basics required to get up and running with development and building great looking responsive websites leveraging Material Design.
Angular Material Design

## References and Links
* https://material.angular.io/
* https://material.io/design/
* https://material.io/design/guidelines-overview/
* https://github.com/angular/components
* https://github.com/ajtowf/angularmaterial
* https://www.youtube.com/c/ajdentowfeek

## Preparation
* Visual Studio Code
* Angular CLI npm install -g @angular/cli
* Typescript https://www.npmjs.com/package/typescript
  npm install -g typescript
* Angular Material
  npm install --save @angular/material
* Angular CDK (component developement kit)
  npm install --save @angular/cdk
* Animations support
  npm install --save @angular/animations
  import BrowserAnimationsModule in App module
* Create material module and import in app module

### first component
* theme: node_modules | @angular | material | pre built themes
* gesture support, hammerjs: npm install --save hammerjs
  and import in app entry point: main.ts
* icons: add to index.html   <link href="https://fonts.googleapis.com/icon?family=Material+Icons"> rel="stylesheet">
  https://google.github.io/material-design-icons/
  import/export MatIconModule in materiaal Module
* import and export all material modules
* import FormsModule in app module
* CSS default to SassCSS SCSS in angular-cli.json (?)
  just rename styles.css to .scss and rename app css to .sccss and edit in ts file
  and changed styles.css to styles.scss in angular.json
  (hmm better when creating app)

### Ready to go.
* Installed tooling
** vs code, NodeJS, Angular CLI, Typescript
* Scaffolded an application
** got started with Angular Material